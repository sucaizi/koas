module.exports = {
  collectCoverageFrom: ['packages/**/*.ts'],
  snapshotSerializers: ['<rootDir>/jest-string'],
  testEnvironment: 'node',
  transform: {
    // [/\.js$/.source]: 'esm',
    [/\.ts$/.source]: 'ts-jest',
  },
};
