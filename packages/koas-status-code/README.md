# Koas Spec Handler

Koas status code automatically sets the response status code based on the success status code
specified in the Open API object.

## Installation

```sh
npm install koa koas-core koas-status-code
```

## Usage

```js
const Koa = require('koa');
const koas = require('koas-core');
const koasStatusCode = require('koas-status-code');

const api = require('./api.json');

async function main() {
  const app = new Koa();
  app.use(await koas(api, [koasStatusCode()]));
}
```
