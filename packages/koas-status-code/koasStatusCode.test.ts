import * as createTestServer from 'koas-supertest';

import * as koasStatusCode from './koasStatusCode';

const spec = {
  openapi: '3.0.2',
  info: {
    title: 'test',
    version: 'test',
  },
  paths: {
    '/create': {
      post: {
        responses: {
          201: { description: '' },
          404: { description: '' },
        },
      },
    },
    '/explicit': {
      get: {
        responses: {
          201: { description: '' },
        },
      },
    },
  },
};

let api;

beforeEach(async () => {
  api = await createTestServer(spec, [
    () => async (ctx, next) => {
      if (ctx.url === '/explicit') {
        ctx.status = 200;
      }
      await next();
    },
    koasStatusCode(),
  ]);
});

it('should set a default success status code', async () => {
  const response = await api.post('/create');
  expect(response.statusCode).toBe(201);
});

it('should ignore when multiple success status codes are defined', async () => {
  const response = await api.get('/conflict', () => {});
  expect(response.statusCode).toBe(404);
});
