import * as Koa from 'koa';
import * as koas from 'koas-core';
import { cloneDeep } from 'lodash';
import { OpenAPIV3 } from 'openapi-types';
import * as supertest from 'supertest';
import * as SwaggerParser from 'swagger-parser';

async function koasSupertest(
  spec: OpenAPIV3.Document,
  middlewares: ((...args: any[]) => Koa.Middleware)[],
  app: Koa = new Koa(),
): Promise<supertest.SuperTest<supertest.Test>> {
  await SwaggerParser.validate(cloneDeep(spec));
  app.use(await koas(spec, middlewares));
  return supertest(app.callback());
}

export = koasSupertest;

namespace koasSupertest {}
