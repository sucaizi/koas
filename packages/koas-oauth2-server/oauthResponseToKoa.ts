import { Context } from 'koa';
import { Response } from 'oauth2-server';

export default function oauthResponseToKoa(response: Response, ctx: Context): void {
  ctx.status = response.status;
  ctx.body = response.body;
  ctx.set(response.headers);
}
