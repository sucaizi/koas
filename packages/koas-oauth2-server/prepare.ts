import { OpenAPIV3 } from 'openapi-types';
import * as OAuth2Server from 'oauth2-server';

export default function prepare(
  spec: OpenAPIV3.Document,
  { validateScope, ...partialModel }: any,
): {
  bySecuritySchemeName: Map<string, OAuth2Server>;
  byTokenURL: Map<string, OAuth2Server>;
} {
  const bySecuritySchemeName = new Map<string, OAuth2Server>();
  const byTokenURL = new Map<string, OAuth2Server>();

  Object.entries(spec.components.securitySchemes).forEach(
    ([name, { flows, type }]: [string, OpenAPIV3.OAuth2SecurityScheme]) => {
      if (type !== 'oauth2') {
        return;
      }
      const grants = [];
      const allowedScopes = new Set();
      const model: OAuth2Server.PasswordModel = {
        ...partialModel,
        async validateScope(user, client, scope: string) {
          if (!scope) {
            return false;
          }
          const scopes = validateScope(
            user,
            client,
            scope.split(' ').filter(s => allowedScopes.has(s)),
          );
          return scopes && scopes.length && scopes;
        },
        async verifyScope(token, scopes: string) {
          const tokenScopes = (token.scope as string).split(' ');
          return scopes.split(' ').every(scope => tokenScopes.includes(scope));
        },
      };
      let tokenUrl: string;

      Object.entries(flows).forEach(([flow, oauthFlowObject]: [string, any]) => {
        const { refreshUrl } = oauthFlowObject;
        if (!tokenUrl) {
          ({ tokenUrl } = oauthFlowObject);
        } else if (oauthFlowObject.tokenUrl !== tokenUrl) {
          throw new Error(
            'Using different token URLs in the same OAuth Flow Object is not supported.',
          );
        }
        if (refreshUrl) {
          if (refreshUrl !== tokenUrl) {
            throw new Error(
              'Using a refresh token URL different from the token URL is not supported.',
            );
          }
          if (!grants.includes('refresh_token')) {
            grants.push('refresh_token');
          }
        }
        switch (flow) {
          case 'authorizationCode':
            grants.push('authorization_code');
            break;
          case 'password':
            grants.push('password');
            break;
          default:
            throw new Error(`Unsupported grant type flow: ${flow}`);
        }
        Object.keys(oauthFlowObject.scopes).forEach(scope => {
          allowedScopes.add(scope);
        });
      });

      const server = new OAuth2Server({
        grants,
        model,
        requireClientAuthentication: { password: false },
      } as any);
      bySecuritySchemeName.set(name, server);
      byTokenURL.set(tokenUrl, server);
    },
  );

  return { bySecuritySchemeName, byTokenURL };
}
