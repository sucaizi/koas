import * as qs from 'querystring';

import { Context } from 'koa';
import * as inflate from 'inflation';
import { Request, Response } from 'oauth2-server';
import * as raw from 'raw-body';

export default async function koaToOAuth2Server(
  ctx: Context,
  parseRequestBody?: boolean,
): Promise<{
  request: Request;
  response: Response;
}> {
  const body = parseRequestBody
    ? qs.parse(
        await raw(inflate(ctx.req), {
          encoding: ctx.request.charset || 'utf8',
          length: ctx.request.length,
        }),
      )
    : null;

  const request = new Request({
    body,
    headers: ctx.headers,
    method: ctx.method,
    query: ctx.query,
  });

  const response = new Response({ headers: {} });

  return { request, response };
}
