import * as Koas from 'koas-core';
import { OAuthError } from 'oauth2-server';

import koaToOAuth2Server from './koaToOAuth2Server';
import oauthResponseToKoa from './oauthResponseToKoa';
import prepare from './prepare';

export = function koasOAuth2Server({
  validateScope = (user, client, scopes) => scopes,
  ...model
}: any): Koas.Plugin {
  return ({ runAlways, spec }) => {
    const { bySecuritySchemeName, byTokenURL } = prepare(spec, {
      validateScope,
      ...model,
    });

    return runAlways(async (ctx, next) => {
      if (byTokenURL.has(ctx.url)) {
        const server = byTokenURL.get(ctx.url);
        const { request, response } = await koaToOAuth2Server(ctx, true);
        try {
          await server.token(request, response);
        } catch (error) {
          if (!(error instanceof OAuthError)) {
            throw error;
          }
        }
        return oauthResponseToKoa(response, ctx);
      }
      const { operationObject } = ctx.openApi;
      if (!operationObject) {
        return next();
      }
      const { security } = operationObject;
      if (!security) {
        return next();
      }
      // XXX This only implements the security definition used by Appsemble.
      const [securityRequirementObject] = security;
      await Promise.all(
        Object.entries(securityRequirementObject).map(async ([name, scopes]) => {
          if (!bySecuritySchemeName.has(name)) {
            throw new Error(`Unknown security scheme ${name}`);
          }
          const { request, response } = await koaToOAuth2Server(ctx);
          const server = bySecuritySchemeName.get(name);
          const { client, scope, user } = await server.authenticate(request, response, {
            scope: scopes.join(' '),
          });
          Object.assign(ctx.state, { client, scope, user });
        }),
      );
      return next();
    });
  };
};
