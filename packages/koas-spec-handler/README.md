# Koas Spec Handler

Koas spec handler exposes the Open API object as a JSON API call.

## Installation

```sh
npm install koa koas-core koas-spec-handler
```

## Usage

```js
const Koa = require('koa');
const koas = require('koas-core');
const koasSpecHandler = require('koas-spec-handler');

const api = require('./api.json');

async function main() {
  const app = new Koa();
  app.use(await koas(api, [koasSpecHandler(/* url */)]));
}
```

## Options

Koas spec handler accepts one single argument. This is a string that specifies on which URL the Open
API object is exposed. By default, it is exposed on `/api.json`.
