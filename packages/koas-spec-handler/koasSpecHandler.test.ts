import * as createTestServer from 'koas-supertest';

import * as koasSpecHandler from './koasSpecHandler';

const spec = {
  openapi: '3.0.2',
  info: {
    title: 'koas-spec-handler',
    version: 'test',
  },
  paths: {},
};

it('should serve the OpenAPI spec on /api.json by default', async () => {
  const api = await createTestServer(spec, [koasSpecHandler()]);
  const response = await api.get('/api.json');
  expect(response.body).toStrictEqual(spec);
});

it('should be possible to define a custom url', async () => {
  const api = await createTestServer(spec, [koasSpecHandler('/custom-url')]);
  const response = await api.get('/custom-url');
  expect(response.body).toStrictEqual(spec);
});

it('should call the next middleware if there is no match', async () => {
  const api = await createTestServer(spec, [koasSpecHandler()]);
  const response = await api.get('/');
  expect(response.status).toBe(404);
});
