import { Middleware } from 'koa';

function koaFunction(fn: Function): Middleware {
  return async (ctx, next) => {
    const result = await fn(ctx, next);
    if (result !== undefined) {
      ctx.body = result;
    }
  };
}

export = koaFunction;

namespace koaFunction {}
