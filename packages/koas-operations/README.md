# Koas Operations

Koas operations maps operation IDs to Koa controller functions.

## Installation

```sh
npm install koa koas-core koas-operations
```

## Usage

```js
const Koa = require('koa');
const koas = require('koas-core');
const koasOperations = require('koas-operations');

const api = require('./api.json');

async function main() {
  const app = new Koa();
  app.use(
    await koas(api, [
      koasOperations({
        operations: {
          // operations
        },
        async fallback(ctx, next) {
          // Handle operation not found
        },
      }),
    ]),
  );
}
```

## Options

- `operations`: A mapping of operation IDs to Koa middleware functions.
- `fallback`: A fallback function. This is called if the operation object doesn’t have an
  `operationId` property or the operation ID doesn’t map to a function in the `operations` object.
