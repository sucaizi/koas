import * as koas from 'koas-core';

function notImplemented(ctx: koas.Context): void {
  ctx.throw(501);
}

function koasOperations({
  operations = {},
  fallback = notImplemented,
}: koasOperations.Options = {}): koas.Plugin {
  return () => async (ctx, next) => {
    const { operationObject } = ctx.openApi;
    const { operationId } = operationObject;
    if (typeof operationId !== 'string') {
      return fallback(ctx, next);
    }
    const operation = operations[operationId];
    if (typeof operation !== 'function') {
      return fallback(ctx, next);
    }
    return operation(ctx, next);
  };
}

export = koasOperations;

declare namespace koasOperations {
  interface Options {
    operations?: { [operationId: string]: koas.Middleware };
    fallback?: koas.Middleware;
  }
}
