# Koas Body Parser

Koas body parser parses and validates request bodies based on the current operation.

## Installation

```sh
npm install koa koas-body-parser koas-core
```

## Usage

```js
const Koa = require('koa');
const koasBodyParser = require('koas-body-parser');
const koas = require('koas-core');

const api = require('./api.json');

async function main() {
  const app = new Koa();
  app.use(
    await koas(api, [
      koasBodyParser({
        // parsers
      }),
    ]),
  );
}
```

## Options

Koas body parser accepts a mapping of mime types to parser functions. Wildcard mime types are
supported as well. The parsers for the following mime types are supported by default:

- `application/json`
- `multipart/form-data`
- `text/plain`

> **Note**: `multipart/form-date` is parsed entirely so it can be validated and used easily later
> on. If the request should be streamed instead, pass a custom `multipart/form-data` parser.
