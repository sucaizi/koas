import * as createTestServer from 'koas-supertest';

import * as koasBodyParser from './koasBodyParser';

const spec = {
  openapi: '3.0.2',
  info: {
    title: 'test',
    version: 'test',
  },
  paths: {
    '/': {
      post: {
        requestBody: {
          content: {
            'application/json': {},
            'text/plain': {},
          },
        },
        responses: {
          default: {
            description: '',
            content: {},
          },
        },
      },
    },
  },
};

let api;
let body;

beforeEach(async () => {
  api = await createTestServer(spec, [
    koasBodyParser(),
    () => async ctx => {
      ({ body } = ctx.request);
    },
  ]);
});

afterEach(() => {
  body = undefined;
});

it('should transform application/json into a JavaScript object', async () => {
  await api
    .post('/')
    .set('Content-Type', 'application/json')
    .send('{"text":"This is json"}');
  expect(body).toStrictEqual({ text: 'This is json' });
});

it('should transform text/plain into a regular string', async () => {
  await api
    .post('/')
    .set('Content-Type', 'text/plain')
    .send('{"text":"This is plain text"}');
  expect(body).toStrictEqual('{"text":"This is plain text"}');
});
