import { Context } from 'koa';
import { OpenAPIV3 } from 'openapi-types';
import * as raw from 'raw-body';

export default (body, mediaTypeObject: OpenAPIV3.MediaTypeObject, ctx: Context): Promise<string> =>
  raw(body, {
    encoding: ctx.request.charset || 'utf8',
    length: ctx.request.length,
  });
