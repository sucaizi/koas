import { Context } from 'koa';
import { OpenAPIV3 } from 'openapi-types';

import formdata from './formdata';
import json from './json';
import plain from './plain';

export interface Parsers {
  [mime: string]: (
    body: any,
    mediaTypeObject: OpenAPIV3.MediaTypeObject,
    ctx: Context,
  ) => Promise<any>;
}

export default {
  'application/json': json,
  'multipart/form-data': formdata,
  'text/plain': plain,
};
