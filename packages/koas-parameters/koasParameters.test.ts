import * as createTestServer from 'koas-supertest';
import { OpenAPIV3 } from 'openapi-types';

import * as koasParameters from './koasParameters';

const spec: OpenAPIV3.Document = {
  openapi: '3.0.2',
  info: {
    title: 'Test server',
    version: 'test',
  },
  paths: {
    '/bool/{value}': {
      parameters: [
        {
          name: 'value',
          in: 'path',
          schema: { type: 'boolean' },
        },
      ],
      get: { responses: {} },
    },
    '/int/{value}': {
      parameters: [
        {
          name: 'value',
          in: 'path',
          schema: { type: 'integer' },
        },
      ],
      get: { responses: {} },
    },
    '/numeric/{value}': {
      parameters: [
        {
          name: 'value',
          in: 'path',
          schema: { type: 'number' },
        },
      ],
      get: { responses: {} },
    },
    '/str/{value}': {
      parameters: [
        {
          name: 'value',
          in: 'path',
          schema: { type: 'string' },
        },
      ],
      get: { responses: {} },
    },
  },
};

let api;
let params;

afterEach(() => {
  params = undefined;
});

describe('defaults', () => {
  beforeEach(async () => {
    api = await createTestServer(spec, [
      koasParameters(),
      () => (ctx, next) => {
        ({ params } = ctx);
        return next();
      },
    ]);
  });

  it('should a boolean value true', async () => {
    await api.get('/bool/true');
    expect(params).toStrictEqual({ value: true });
  });

  it('should a boolean value false', async () => {
    await api.get('/bool/false');
    expect(params).toStrictEqual({ value: false });
  });

  it('should parse an integer', async () => {
    await api.get('/int/42');
    expect(params).toStrictEqual({ value: 42 });
  });

  it('should parse a number', async () => {
    await api.get('/numeric/13.37');
    expect(params).toStrictEqual({ value: 13.37 });
  });

  it('should leave a string as-is', async () => {
    await api.get('/str/1337');
    expect(params).toStrictEqual({ value: '1337' });
  });
});

describe('custom parsers', () => {
  beforeEach(async () => {
    api = await createTestServer(spec, [
      koasParameters({ parsers: { string: value => `${value}-postfix` } }),
      () => (ctx, next) => {
        ({ params } = ctx);
        return next();
      },
    ]);
  });

  it('should leave a string as-is', async () => {
    await api.get('/str/value');
    expect(params).toStrictEqual({ value: 'value-postfix' });
  });
});
