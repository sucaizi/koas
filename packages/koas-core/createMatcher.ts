import { OpenAPIV3 } from 'openapi-types';

/**
 *
 */
export type matcherFunction = (url: string) => { [name: string]: string };

/**
 * Create a matcher function for an OpenAPI path template.
 *
 * @param pathTemplate
 * @param pathParameters
 */
export default function createMatcher(
  pathTemplate: string,
  pathParameters: OpenAPIV3.ParameterObject[] = [],
): matcherFunction {
  const arrayNames = pathParameters
    .filter(({ schema = {} }: any) => schema.type === 'array')
    .map(({ name }) => name);
  const names: string[] = [];
  const pathRegex = new RegExp(
    `^${pathTemplate.replace(/{(\w+)}/gi, (match, name) => {
      names.push(name);
      return arrayNames.includes(name) ? '(.*)' : '([^/]+)';
    })}$`,
    'i',
  );

  return url => {
    const urlMatch = url.match(pathRegex);
    if (!urlMatch) {
      return null;
    }
    return names.reduce((acc, name, index) => {
      acc[name] = decodeURI(urlMatch[index + 1]);
      return acc;
    }, {});
  };
}
