import { Context } from 'koa';
import * as createServer from 'koas-supertest';
import { OpenAPIV3 } from 'openapi-types';
import * as supertest from 'supertest';

function noop(ctx: Context, next: () => Promise<any>): Promise<any> {
  return next();
}

describe('koas', () => {
  const spec: OpenAPIV3.Document = {
    openapi: '3.0.2',
    info: {
      title: 'Test server',
      version: 'test',
    },
    components: {
      schemas: {
        foo: { type: 'string' },
        bar: { $ref: '#/components/schemas/foo' },
      },
    },
    paths: {
      '/': {
        get: {
          responses: {},
        },
      },
    },
  };
  let api: supertest.SuperTest<supertest.Test>;
  let context: Context;
  let injectedArgs: any[];
  let middlewares: jest.Mock<Promise<any>>[];

  beforeEach(async () => {
    middlewares = [jest.fn(noop), jest.fn(noop)];
    api = await createServer(spec, [
      args => async (ctx: Context, next: () => Promise<any>) => {
        injectedArgs = args;
        context = ctx;
        await next();
      },
      ...middlewares.map(m => () => m),
    ]);
  });

  it('should call middleware', async () => {
    await api.get('/');
    expect(middlewares[0]).toHaveBeenCalledTimes(1);
    expect(middlewares[0]).toHaveBeenCalledTimes(1);
  });

  it('should inject the raw and dereferenced OpenAPI spec', async () => {
    await api.get('/');
    expect(injectedArgs).toMatchObject({
      spec: {
        openapi: '3.0.2',
        info: {
          title: 'Test server',
          version: 'test',
        },
        components: {
          schemas: {
            foo: { type: 'string' },
            bar: { type: 'string' },
          },
        },
        paths: {
          '/': {
            get: {
              responses: {},
            },
          },
        },
      },
      rawSpec: {
        openapi: '3.0.2',
        info: {
          title: 'Test server',
          version: 'test',
        },
        components: {
          schemas: {
            foo: { type: 'string' },
            bar: { $ref: '#/components/schemas/foo' },
          },
        },
        paths: {
          '/': {
            get: {
              responses: {},
            },
          },
        },
      },
      runAlways: expect.any(Function),
    });
  });

  it('should configure openApi related fields for middleware', async () => {
    await api.get('/');
    expect(context.openApi.pathItemObject).toStrictEqual(spec.paths['/']);
    expect(context.openApi.operationObject).toStrictEqual(spec.paths['/'].get);
  });
});
