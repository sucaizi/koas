import * as Koa from 'koa';
import * as compose from 'koa-compose';
import { cloneDeep } from 'lodash';
import * as RefParser from 'json-schema-ref-parser';
import { OpenAPIV3 } from 'openapi-types';

import createMatcher, { matcherFunction } from './createMatcher';
import createDefaultValidator, { validateFunction } from './createDefaultValidator';
import SchemaValidationError_ from './SchemaValidationError';

const methods = new Set(['delete', 'get', 'head', 'options', 'patch', 'post', 'put', 'trace']);

/**
 * This symbol is used internally to specify a middleware should always be run.
 */
const RUN_ALWAYS = Symbol('This middleware is always run');

/**
 * Mark that middleware always needs to run, even if there is no matching OpenAPI operation.
 *
 * @param middleware The middleware to mark.
 * @returns The marked middleware itself.
 */
function markRunAlways(middleware: koas.Middleware): koas.Middleware {
  // eslint-disable-next-line no-param-reassign
  middleware[RUN_ALWAYS] = true;
  return middleware;
}

async function koas(
  spec: OpenAPIV3.Document,
  middlewares: koas.Plugin[] = [],
  { createValidator = createDefaultValidator }: koas.AdvancedOptions = {},
): Promise<koas.Middleware> {
  const dereferencedSpec: OpenAPIV3.Document = await RefParser.dereference(cloneDeep(spec));
  const matchers = Object.entries(dereferencedSpec.paths).map<
    [matcherFunction, OpenAPIV3.PathItemObject]
  >(([pathTemplate, pathItemObject]) => {
    const matcher = createMatcher(
      pathTemplate,
      pathItemObject.parameters as OpenAPIV3.ParameterObject[],
    );
    return [matcher, pathItemObject];
  });
  const validate = await createValidator(spec);

  const injected = middlewares.map(middleware =>
    middleware({
      rawSpec: spec,
      runAlways: markRunAlways,
      spec: dereferencedSpec,
      validate,
    }),
  );
  const composed = compose(injected);
  const runAlways = compose(injected.filter(middleware => middleware[RUN_ALWAYS]));

  return (ctx, next) => {
    let params: { [name: string]: string };
    const match = matchers.find(([matcher]) => {
      params = matcher(ctx.path);
      return !!params;
    });
    ctx.openApi = { openApiObject: spec, validate };
    if (!match) {
      return runAlways(ctx, next);
    }
    const [, pathItemObject] = match;
    ctx.openApi.pathItemObject = pathItemObject;
    ctx.params = params;
    const method = ctx.method.toLowerCase();
    if (!methods.has(method)) {
      return runAlways(ctx, next);
    }
    const operationObject = pathItemObject[method];
    if (!operationObject) {
      return runAlways(ctx, next);
    }
    ctx.openApi.operationObject = operationObject;
    return composed(ctx, next);
  };
}

export = koas;

namespace koas {
  export const SchemaValidationError = SchemaValidationError_;

  export interface AdvancedOptions {
    createValidator?: (spec?: OpenAPIV3.Document) => validateFunction;
  }

  export type Context = Koa.Context & {
    openApi?: OpenAPIContext;
  };

  export type Middleware<StateT = any, CustomT = {}> = compose.Middleware<
    Koa.ParameterizedContext<StateT, CustomT & koas.Context>
  >;

  export interface OpenAPIContext {
    operationObject?: OpenAPIV3.OperationObject;
    openApiObject: OpenAPIV3.Document;
    pathItemObject?: OpenAPIV3.PathItemObject;
    validate: validateFunction;
  }

  /**
   * A function that takes Koas options and returns Koa Middleware.
   */
  export type Plugin<StateT = any, CustomT = {}> = (
    options: koas.PluginOptions<StateT, CustomT>,
  ) => koas.Middleware<StateT, CustomT>;

  export interface PluginOptions<StateT = any, CustomT = {}> {
    rawSpec: OpenAPIV3.Document;
    runAlways: (middleware: koas.Middleware<StateT, CustomT>) => koas.Middleware<StateT, CustomT>;
    spec: OpenAPIV3.Document;
    validate: validateFunction;
  }
}
