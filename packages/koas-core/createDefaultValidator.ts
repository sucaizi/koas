import { OpenAPIV3 } from 'openapi-types';
import * as Validator from 'z-schema';

import SchemaValidationError from './SchemaValidationError';

Validator.registerFormat('binary', () => true);

export type validateFunction = (data: any, schema: OpenAPIV3.SchemaObject) => Promise<boolean>;

export default function createDefaultValidator(): validateFunction {
  const validator = new Validator({
    assumeAdditional: true,
    breakOnFirstError: false,
    reportPathAsArray: true,
  });
  return (data, schema) => {
    return new Promise((resolve, reject) => {
      validator.validate(data, schema, (errors, valid) => {
        if (valid) {
          resolve(true);
        } else {
          reject(new SchemaValidationError('JSON schema validation failed', errors));
        }
      });
    });
  };
}
