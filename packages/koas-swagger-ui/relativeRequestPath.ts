export default function relativeRequestPath(root: string, file: string): string {
  if (root.endsWith('/')) {
    return `${root}${file}`;
  }
  return root
    .split('/')
    .slice(0, -1)
    .concat(file)
    .join('/');
}
