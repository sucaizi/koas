import relativeRequestPath from './relativeRequestPath';

it.each([
  ['/', 'index.css', '/index.css'],
  ['/foo', 'index.css', '/index.css'],
  ['/foo/', 'index.css', '/foo/index.css'],
  ['/foo/bar', 'index.css', '/foo/index.css'],
  ['/foo/bar/', 'index.css', '/foo/bar/index.css'],
])('%s and %s should resolve to %s', (root, file, expected) => {
  const result = relativeRequestPath(root, file);
  expect(result).toBe(expected);
});
