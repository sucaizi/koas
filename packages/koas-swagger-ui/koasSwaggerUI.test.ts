import { readFileSync } from 'fs';
import { join } from 'path';

import * as koasSpecHandler from 'koas-spec-handler';
import * as createTestServer from 'koas-supertest';
import { absolutePath } from 'swagger-ui-dist';

import * as koasSwaggerUI from './koasSwaggerUI';

const assets = [
  'favicon-16x16.png',
  'favicon-32x32.png',
  'swagger-ui-bundle.js',
  'swagger-ui-bundle.js.map',
  'swagger-ui.css',
  'swagger-ui.css.map',
  'swagger-ui-standalone-preset.js',
  'swagger-ui-standalone-preset.js.map',
].reduce((acc, filename) => {
  acc[filename] = readFileSync(join(absolutePath(), filename));
  return acc;
}, {});

const spec = {
  openapi: '3.0.2',
  info: {
    title: 'Test server',
    version: 'test',
  },
  paths: {},
};

describe.each([
  ['/', '/'],
  ['/foo', '/'],
  ['/foo/', '/foo/'],
  ['/foo/bar', '/foo/'],
  ['/foo/bar/', '/foo/bar/'],
])('given url %s', (url, prefix) => {
  it.each([
    ['favicon-16x16.png', 'image/png'],
    ['favicon-32x32.png', 'image/png'],
    ['swagger-ui-bundle.js', 'application/javascript'],
    ['swagger-ui-bundle.js.map', 'application/json'],
    ['swagger-ui.css', 'text/css'],
    ['swagger-ui.css.map', 'application/json'],
  ])(`should serve ${prefix}%s using mime type %s`, async (p, mime) => {
    const api = await createTestServer(spec, [koasSpecHandler(), koasSwaggerUI({ url })]);
    let body;
    const response = await api.get(`${prefix}${p}`).parse((res, cb) => {
      const chunks = [];
      res
        .on('data', data => {
          chunks.push(data);
        })
        .on('error', cb)
        .on('end', () => {
          body = Buffer.concat(chunks);
          cb(null, res);
        });
    });
    expect(body.equals(assets[p])).toBe(true);
    expect(response.type).toBe(mime);
  });
});

describe('defaults', () => {
  it('should render index.html', async () => {
    const api = await createTestServer(spec, [koasSpecHandler(), koasSwaggerUI()]);
    const response = await api.get('/');
    expect(response.type).toBe('text/html');
    expect(response.text).toMatchSnapshot();
  });
});

describe('trailing slash', () => {
  it('should render index.html', async () => {
    const api = await createTestServer(spec, [
      koasSpecHandler(),
      koasSwaggerUI({ url: '/prefix/' }),
    ]);
    const response = await api.get('/prefix/');
    expect(response.type).toBe('text/html');
    expect(response.text).toMatchSnapshot();
  });
});

describe('no trailing slash', () => {
  it('should render index.html', async () => {
    const api = await createTestServer(spec, [
      koasSpecHandler(),
      koasSwaggerUI({ url: '/prefix' }),
    ]);
    const response = await api.get('/prefix');
    expect(response.type).toBe('text/html');
    expect(response.text).toMatchSnapshot();
  });
});
