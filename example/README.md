Welcome to the Pet Store example.

The source code for this example can be found on the [Koas example repository][].

[koas example repository]: https://gitlab.com/remcohaszing/koas/tree/master/example
