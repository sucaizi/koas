const yargs = require('yargs');

module.exports = () =>
  yargs
    .usage('Usage:\n  npm start')
    .option('database', {
      desc: 'The database URL to use.',
      default: 'sqlite://:memory:',
    })
    .option('port', {
      desc: 'The port on which to host the server.',
      type: 'number',
      default: 3333,
    }).argv;
