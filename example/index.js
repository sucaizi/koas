const argv = require('./argv');
const { start } = require('./server');

if (module === require.main) {
  start(argv()).catch(error => {
    // eslint-disable-next-line no-console
    console.error(error);
    process.exit(error.code || 1);
  });
}
