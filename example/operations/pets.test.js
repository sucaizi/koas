const supertest = require('supertest');

const { createServer } = require('../server');

let api;

beforeEach(async () => {
  api = supertest((await createServer({ database: 'sqlite://:memory:' })).callback());
});

it('should be possible to create pets', async () => {
  const { body, statusCode } = await api.post('/pets').send({ species: 'cat', name: 'Mittens' });
  expect(body).toStrictEqual({ id: expect.any(Number), species: 'cat', name: 'Mittens' });
  expect(statusCode).toBe(201);
});

it('should be possible to retrieve pets', async () => {
  const { body: created } = await api.post('/pets').send({ species: 'cat', name: 'Mittens' });
  const { body: retrieved, statusCode } = await api.get(`/pets/${created.id}`);
  expect(retrieved).toStrictEqual(created);
  expect(statusCode).toBe(200);
});

it('should be possible to create and list', async () => {
  const { body: cat } = await api.post('/pets').send({ species: 'cat', name: 'Mittens' });
  const { body: dog } = await api.post('/pets').send({ species: 'dog', name: 'Brian' });
  const { body, statusCode } = await api.get('/pets');
  expect(body).toStrictEqual([cat, dog]);
  expect(statusCode).toBe(200);
});
