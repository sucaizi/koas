async function createPet(ctx) {
  const { Pet } = ctx.models;
  return Pet.create(ctx.request.body);
}

async function getPets(ctx) {
  const { Pet } = ctx.models;
  return Pet.findAll({ raw: true });
}

async function getPetById(ctx) {
  const { Pet } = ctx.models;
  const { petId } = ctx.params;
  return Pet.findByPk(petId, { raw: true });
}

async function patchPet(ctx) {
  const { Pet } = ctx.models;
  const { petId } = ctx.params;
  const pet = await Pet.findByPk(petId);
  pet.set(ctx.request.body);
  await pet.save();
  return pet.dataValues;
}

async function updatePet(ctx) {
  const { Pet } = ctx.models;
  const { petId } = ctx.params;
  const pet = await Pet.findByPk(petId);
  pet.set(ctx.request.body);
  await pet.save();
  return pet.dataValues;
}

async function deletePet(ctx) {
  const { Pet } = ctx.models;
  const { petId } = ctx.params;
  const pet = await Pet.findByPk(petId);
  await pet.destroy();
}

module.exports = { createPet, getPets, getPetById, patchPet, updatePet, deletePet };
