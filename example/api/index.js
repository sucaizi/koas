const components = require('./components');
const info = require('./info');
const paths = require('./paths');
const tags = require('./tags');

module.exports = {
  openapi: '3.0.1',
  components,
  info,
  paths,
  tags,
};
