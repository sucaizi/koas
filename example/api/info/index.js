const fs = require('fs');
const path = require('path');

const parseAuthor = require('parse-author');

const pkg = require('../../package.json');

module.exports = {
  title: pkg.name,
  version: pkg.version,
  contact: parseAuthor(pkg.author),
  description: fs.readFileSync(path.resolve(__dirname, '../../README.md'), 'utf8'),
};
