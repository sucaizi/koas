const { cloneDeep } = require('lodash');
const SwaggerParser = require('swagger-parser');

const api = require('.');

it('should be a valid Swagger spec', async () => {
  await expect(SwaggerParser.validate(cloneDeep(api))).resolves.toBeDefined();
});
