module.exports = {
  '/pets': {
    post: {
      operationId: 'createPet',
      tags: ['pet'],
      requestBody: {
        $ref: '#/components/requestBodies/Pet',
      },
      responses: {
        201: {
          $ref: '#/components/responses/Pet',
        },
        default: {
          $ref: '#/components/responses/GeneralError',
        },
      },
    },
    get: {
      operationId: 'getPets',
      tags: ['pet'],
      responses: {
        200: {
          description: 'A list of pets.',
          content: {
            'application/json': {
              schema: {
                type: 'array',
                items: {
                  $ref: '#/components/schemas/Pet',
                },
              },
            },
            'text/yaml': {
              schema: {
                type: 'array',
                items: {
                  $ref: '#/components/schemas/Pet',
                },
              },
            },
          },
        },
        default: {
          $ref: '#/components/responses/GeneralError',
        },
      },
    },
  },
  '/pets/{petId}': {
    parameters: [
      {
        name: 'petId',
        in: 'path',
        required: true,
        description: 'The pet identifier.',
        schema: {
          type: 'number',
        },
      },
    ],
    get: {
      operationId: 'getPetById',
      tags: ['pet'],
      responses: {
        '200': {
          $ref: '#/components/responses/Pet',
        },
        default: {
          $ref: '#/components/responses/GeneralError',
        },
      },
    },
    patch: {
      operationId: 'patchPet',
      tags: ['pet'],
      requestBody: {
        $ref: '#/components/requestBodies/Pet',
      },
      responses: {
        200: {
          $ref: '#/components/responses/Pet',
        },
        default: {
          $ref: '#/components/responses/GeneralError',
        },
      },
    },
    put: {
      operationId: 'updatePet',
      tags: ['pet'],
      requestBody: {
        $ref: '#/components/requestBodies/Pet',
      },
      responses: {
        200: {
          $ref: '#/components/responses/Pet',
        },
        default: {
          $ref: '#/components/responses/GeneralError',
        },
      },
    },
    delete: {
      operationId: 'deletePet',
      tags: ['pet'],
      responses: {
        204: {
          description: 'If the pet has been deleted succesfully.',
        },
        default: {
          $ref: '#/components/responses/GeneralError',
        },
      },
    },
  },
};
