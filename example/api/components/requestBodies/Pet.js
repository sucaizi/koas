module.exports = {
  required: true,
  content: {
    'application/json': {
      schema: {
        $ref: '#/components/schemas/Pet',
      },
    },
    'multipart/form-data': {
      schema: {
        type: 'object',
        properties: {
          data: {
            $ref: '#/components/schemas/Pet',
          },
          photo: {
            type: 'string',
            format: 'binary',
          },
        },
      },
    },
    'text/yaml': {
      schema: {
        $ref: '#/components/schemas/Pet',
      },
    },
  },
};
