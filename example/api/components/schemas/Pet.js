module.exports = {
  type: 'object',
  example: {
    name: 'Mittens',
    species: 'cat',
  },
  required: ['name', 'species'],
  properties: {
    id: {
      type: 'number',
      readOnly: true,
      description: 'The generated unique identifier.',
    },
    name: {
      type: 'string',
      description: 'The name of the pet.',
    },
    species: {
      type: 'string',
      description: 'The species of the pet.',
    },
  },
};
