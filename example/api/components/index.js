const requestBodies = require('./requestBodies');
const responses = require('./responses');
const schemas = require('./schemas');
const securitySchemes = require('./securitySchemes');

module.exports = { requestBodies, responses, schemas, securitySchemes };
