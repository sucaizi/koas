module.exports = {
  type: 'oauth2',
  flows: {
    implicit: {
      authorizationUrl: '/token',
      refreshUrl: '/token',
      scopes: {
        'write:pets': 'Modify pets',
        'read:pets': 'Read pet data',
      },
    },
    password: {
      tokenUrl: '/token',
      refreshUrl: '/token',
      scopes: {
        'write:pets': 'Modify pets',
        'read:pets': 'Read pet data',
      },
    },
    clientCredentials: {
      tokenUrl: '/token',
      refreshUrl: '/token',
      scopes: {
        'write:pets': 'Modify pets',
        'read:pets': 'Read pet data',
      },
    },
    authorizationCode: {
      authorizationUrl: '/token',
      tokenUrl: '/token',
      refreshUrl: '/token',
      scopes: {
        'write:pets': 'Modify pets',
        'read:pets': 'Read pet data',
      },
    },
  },
};
