module.exports = {
  description: 'A successful pet response.',
  content: {
    'application/json': {
      schema: {
        $ref: '#/components/schemas/Pet',
      },
    },
    'text/yaml': {
      schema: {
        $ref: '#/components/schemas/Pet',
      },
    },
  },
};
