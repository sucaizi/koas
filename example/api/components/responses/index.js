const GeneralError = require('./GeneralError');
const Pet = require('./Pet');

module.exports = { GeneralError, Pet };
