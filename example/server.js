const Koa = require('koa');
const fn = require('koa-function');
const logger = require('koa-logger');
const koas = require('koas-core');
const koasBodyParser = require('koas-body-parser');
const koasSerializer = require('koas-serializer');
const koasOperations = require('koas-operations');
const koasParameters = require('koas-parameters');
const koasSpecHandler = require('koas-spec-handler');
const koasStatusCode = require('koas-status-code');
const koasSwaggerUI = require('koas-swagger-ui');
const { mapValues } = require('lodash');

const api = require('./api');
const models = require('./models');
const operations = require('./operations');

async function createServer({ database }) {
  const app = new Koa();
  app.context.models = await models(database);
  if (process.env.NODE_ENV !== 'test') {
    app.use(logger());
  }
  app.use(
    await koas(api, [
      koasSpecHandler(),
      koasSwaggerUI(),
      koasStatusCode(),
      koasSerializer(),
      koasParameters(),
      koasBodyParser(),
      koasOperations({ operations: mapValues(operations, fn) }),
    ]),
  );
  return app;
}

async function start(argv) {
  const { port } = argv;
  const app = await createServer(argv);
  app.listen(port, () => {
    // eslint-disable-next-line no-console
    console.log(`Listening on http://localhost:${port}`);
  });
}

module.exports = { createServer, start };
