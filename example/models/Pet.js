module.exports = (sequelize, DataTypes) =>
  sequelize.define('Pet', {
    id: {
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER,
    },
    name: DataTypes.STRING,
    species: DataTypes.STRING,
  });
