const Sequelize = require('sequelize');

module.exports = async connectionURL => {
  const sequelize = new Sequelize(connectionURL, { define: { timestamps: false }, logging: false });
  sequelize.import('./Pet');
  await sequelize.sync();
  return sequelize.models;
};
